<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ['user_id', 'projectname', 'type', 'location'];

    protected $casts = [
    	'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @param bool $forUpdate
     * @return array
     */
    public function getValidationRules($forUpdate = false)
    {
    	$createRule = [
    		'projectname' => 'required | max:200',
    		'type' => 'required | max:200',
    		'location' => 'required|max:200',
    	];
        
        $updateRule = [
            'projectname' => 'required|max:200',
            'type' => 'required|max:200',
            'location' => 'required|max:200',
        ];

        return $forUpdate ? $updateRule : $createRule;
    }

    public function device()
    {
    	return $this->hasMany(Device::class);
    }


    public function user()
    {
    	return $this->belongsTo(User::class)->select([
    		'id', 'name', 'remember_token', 'created_at'
    	]);
    }
}
