<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\User;
use App\Models\Project;
use App\Models\Device;
use App\Models\Status;
use App\Charts\Chart;
use DB;
 
class ChartController extends Controller
{
    public function scores() {

        $query = TestScore::orderBy('testType', 'asc')->get();
        $yearSelect = 2000;

        $chart1 = Charts::database($query, 'line', 'highcharts')
            ->title('No of Tests by Level for ' . $yearSelect)
            ->elementLabel('# of students in ' . $yearSelect)
            ->labels([
                'Level 1',
                'Level 2',
                'Level 3',
                'Level 4',
                'Level 5',
                'Level 6',
                'Level 7',
                'Level 8',
            ])
            ->values([
                count($query->where('testLevel', 0)->where('testedYear', $yearSelect)),
                count($query->where('testLevel', 1)->where('testedYear', $yearSelect)),
                count($query->where('testLevel', 2)->where('testedYear', $yearSelect)),
                count($query->where('testLevel', 3)->where('testedYear', $yearSelect)),
                count($query->where('testLevel', 4)->where('testedYear', $yearSelect)),
                count($query->where('testLevel', 5)->where('testedYear', $yearSelect)),
                count($query->where('testLevel', 6)->where('testedYear', $yearSelect)),
                count($query->where('testLevel', 7)->where('testedYear', $yearSelect))
            ])
            ->dimensions(1000,500)
            ->responsive(true);

           // Test - Redo later - Do more here
            $data1 = TestScore::orderBy('testType', 'asc')
                ->where('testedYear', 2000)
                ->where('testLevel', 2)
                ->where('testScore', '>', 50)
                ->get();

            $data2 = TestScore::orderBy('testType', 'asc')
                ->where('testedYear', 2005)
                ->where('testLevel', 2)
                ->where('testScore', '>', 50)
                ->get();

            $data3 = TestScore::orderBy('testType', 'asc')
                ->where('testedYear', 2010)
                ->where('testLevel', 2)
                ->where('testScore', '>', 50)
                ->get();

            $data4 = TestScore::orderBy('testType', 'asc')
                ->where('testedYear', 2015)
                ->where('testLevel', 2)
                ->where('testScore', '>', 50)
                ->get();

            $data = [
                'chart1' => $chart1,
            ];

        return view('charts/testscores', $data);

    }
}