<?php

namespace App\Http\Controllers;

use App\Models\Device;
use App\Models\User;
use App\Models\Project;
use App\Models\Status;
use App\Repos\Repository;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;
use Validator;

class DevicesManagementController extends Controller
{
    


    public function getDeviceByProjectname($Devicename)
    {
        return Project::with('device')->wherename($projectname)->firstOrFail();
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     $user_id = Auth::id();

    //     //$projects = User::find($user_id)->project->paginates(env('PROJECT_LIST_PAGINATION_SIZE'));

    //     $devices = Device::where('user_id',$user_id)->paginate(env('PROJECT_LIST_PAGINATION_SIZE'));   

    //     return view('devicesmanagement.show-devices', compact('devices'));
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $project = Project::findOrFail($id);
        return view('devicesmanagement.create-device');
    }

    public function form_input($id){
        //$projects = Project::where(['id'=> $id]);
        return view('devicesmanagement.create-device',compact('id'));
    }

    public function proses_dev(Request $request){
        
        $validator = Validator::make($request->all(),
            [
                'devicename'           => 'required|unique:devices',
                
                
            ],
            [
                'devicename.unique'        => trans('devicesmanagement.deviceNameTaken'),
                'devicename.required'      => trans('devicesmanagement.deviceNameRequired'),
                
                                
            ]
        );

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $id = 'project_id';

        $device = Device::create([
            'devicename'          => $request->input('devicename'),
            'user_id'             => Auth::id(),
            'project_id'          => $request->input('project_id')
            
               
        ]);
        return redirect('projects/' . $id)->with('success', trans('devicesmanagement.createSuccess'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	
        $validator = Validator::make($request->all(),
            [
                'devicename'           => ['required',Rule::unique('devices')->where(function ($query) { $project_id = 'project_id';
                                                return $query->where('project_id', $project_id); })],
                
                
            ],
            [
                'devicename.unique'        => trans('devicesmanagement.deviceNameTaken'),
                'devicename.required'      => trans('devicesmanagement.deviceNameRequired'),
                
                                
            ]
        );

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $id = $request->input('project_id');
                      
        $device = Device::create([
            'devicename'          => $request->input('devicename'),
            'user_id'             => Auth::id(),
            'project_id'          => $request->input('project_id'),
            
            
            
        ]);

        
        return redirect('projects/'.$id)->with('success', trans('devicesmanagement.createSuccess'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) 
    {
        $currentuser = Auth::user();
        $device = Device::find($id);
        $user = User::find($id);
        $statuses = Status::where('device_id',$device->id)->orderByDesc('created_at')->paginate(env('PROJECT_LIST_PAGINATION_SIZE'));

        $coba = json_encode(Status::where('device_id',$device->id)->orderByDesc('created_at')->take(6)->get());
        if ($currentuser->roles->first()->name == 'Admin' ) {
            
            $statuses;
            $coba;
            return view('statusesmanagement.show-statuses', compact('coba', 'statuses', 'device'))->withUser($user);
        }
            if ($device->user_id == $currentuser->id) {
                $statuses;
                $coba;
                return view('statusesmanagement.show-statuses', compact('coba', 'statuses', 'device'))->withUser($user);
            }
        return view('errors.404');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     $device = Device::findOrFail($id);
        

    //     $data = [
    //         'device'        => $device,
            
    //     ];

    //     return view('devicesmanagement.edit-device')->with($data);
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $device = Device::find($id);

        $validator = Validator::make($request->all(), [
                
                'onoff'         => 'integer',
                
        ]);
        

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $device->onoff = $request->input('onoff');
        
        $device->save();

        return back()->with('success', trans('devicesmanagement.updateSuccess'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currentuser = Auth::user();
        $device = Device::find($id);
        $project = $device->project_id;

        if ($currentuser->id == $device->user_id) {
            $device->status()->delete();
            $device->delete();

            return redirect('projects/'.$project)->with('success', trans('devicesmanagement.deleteSuccess'));
        }

        return back()->with('error', trans('devicesmanagement.deleteSelfError'));
    }

    /**
     * Method to search the devices.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $searchTerm = $request->input('device_search_box');
        $searchRules = [
            'device_search_box' => 'required|string|max:255',
        ];
        $searchMessages = [
            'device_search_box.required' => 'Search term is required',
            'device_search_box.string'   => 'Search term has invalid characters',
            'device_search_box.max'      => 'Search term has too many characters - 255 allowed',
        ];

        $validator = Validator::make($request->all(), $searchRules, $searchMessages);

        if ($validator->fails()) {
            return response()->json([
                json_encode($validator),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $results = Device::where('id', 'like', $searchTerm.'%')
                            ->orWhere('devicename', 'like', $searchTerm.'%')->get();

        // Attach roles to results
        foreach ($results as $result) {
            $roles = [
                'roles' => $result->roles,
            ];
            $result->push($roles);
        }

        return response()->json([
            json_encode($results),
        ], Response::HTTP_OK);
    }
}
