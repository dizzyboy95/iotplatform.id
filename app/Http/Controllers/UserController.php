<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use App\Models\Project;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $user_id = Auth::id();

        $projects = Project::where('user_id',$user_id)->paginate(env('PROJECT_LIST_PAGINATION_SIZE'));

        if ($user->isAdmin()) {
            return view('pages.admin.home');
        }

        return view('pages.user.home', compact('projects'));
    }

}
