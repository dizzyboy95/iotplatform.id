<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\User;
use App\Models\Device;
use App\Repos\Repository;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use jeremykenedy\LaravelRoles\Models\Role;
use Validator;
use Illuminate\Validation\Rule;

class ProjectsManagementController extends Controller
{
    /**
     * @var Repository
     */
    protected $model;
    /**
     * ChannelController constructor.
     *
     * @param Channel $channel
     */
    public function __construct(Project $project)
    {
        $this->model = new Repository( $project );

        $this->middleware('auth');
    }

    public function getProjectByProjectname($projectname)
    {
        return Project::with('device')->wherename($projectname)->firstOrFail();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::id();

        $projects = Project::where('user_id',$user_id)->orderByDesc('created_at')->paginate(env('PROJECT_LIST_PAGINATION_SIZE'));
        
        return view('projectsmanagement.home-project', compact('projects'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('projectsmanagement.create-project');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	$validator = Validator::make($request->all(),
            [
                'projectname'           => ['required',Rule::unique('projects')->where(function ($query) { 
						return $query->where('user_id', Auth::id()); })],
                'type'                  => 'required',
                'location'              => 'required',
                
            ],
            [
                'projectname.unique'    => trans('projectsmanagement.projectNameTaken'),
                'projectname.required'  => trans('projectsmanagement.projectNameRequired'),
                'type.required'         => trans('projectsmanagement.typeRequired'),
                'location.required'     => trans('projectsmanagement.locationRequired'),
                                
            ]
        );

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $project = Project::create([
            'projectname'         => $request->input('projectname'),
            'type'                => $request->input('type'),
            'location'            => $request->input('location'),
            'user_id'             => Auth::id(),
                       
            
        ]);

        
        return redirect('projects')->with('success', trans('projectsmanagement.createSuccess'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $currentuser = Auth::user();
        $project = Project::find($id);
        $user = User::find($id);

        if ($currentuser->roles->first()->name == 'Admin' ) {
            $devices = Device::where('project_id',$project->id)->orderByDesc('created_at')->paginate(env('PROJECT_LIST_PAGINATION_SIZE'));
            return view('devicesmanagement.show-devices', compact('project', 'devices'))->withUser($user);

        }
            if ($project->user_id == $currentuser->id) {
            $devices = Device::where('project_id',$project->id)->orderByDesc('created_at')->paginate(env('PROJECT_LIST_PAGINATION_SIZE'));
            return view('devicesmanagement.show-devices', compact('project', 'devices'))->withUser($user);
            }

        return view('errors.404');
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::findOrFail($id);
        

        $data = [
            'project'        => $project,
            
        ];

        return view('projectsmanagement.edit-project')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $project = Project::find($id);


        $validator = Validator::make($request->all(), [
                
                'type'            => 'required',
                'location'        => 'required',
            ]);
        

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        
        $project->type = $request->input('type');
        $project->location = $request->input('location');

        
        $project->save();

        return back()->with('success', trans('projectsmanagement.updateSuccess'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currentuser = Auth::user();
        $project = Project::find($id);

        if ($currentuser->id == $project->user_id) {
            $project->device()->delete();
            $project->delete();

            return redirect('projects')->with('success', trans('projectsmanagement.deleteSuccess'));
        }

        return back()->with('error', trans('projectsmanagement.deleteSelfError'));
    }

    /**
     * Method to search the users.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $searchTerm = $request->input('project_search_box');
        $searchRules = [
            'project_search_box' => 'required|string|max:255',
        ];
        $searchMessages = [
            'project_search_box.required' => 'Search term is required',
            'project_search_box.string'   => 'Search term has invalid characters',
            'project_search_box.max'      => 'Search term has too many characters - 255 allowed',
        ];

        $validator = Validator::make($request->all(), $searchRules, $searchMessages);

        if ($validator->fails()) {
            return response()->json([
                json_encode($validator),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $results = Project::where('id', 'like', $searchTerm.'%')
                            ->orWhere('projectname', 'like', $searchTerm.'%')
                            ->orWhere('type', 'like', $searchTerm.'%')
                            ->orWhere('location', 'like', $searchTerm. '%') ->get();

        // Attach roles to results
        foreach ($results as $result) {
            $roles = [
                'roles' => $result->roles,
            ];
            $result->push($roles);
        }

        return response()->json([
            json_encode($results),
        ], Response::HTTP_OK);
    }
}
