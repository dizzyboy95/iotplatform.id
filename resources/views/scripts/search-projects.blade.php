<script>
    $(function() {
        var cardTitle = $('#card_title');
        var projectsTable = $('#projects_table');
        var resultsContainer = $('#search_results');
        var projectsCount = $('#project_count');
        var clearSearchTrigger = $('.clear-search');
        var searchform = $('#search_projects');
        var searchformInput = $('#project_search_box');
        var projectPagination = $('#project_pagination');
        var searchSubmit = $('#search_trigger');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        searchform.submit(function(e) {
            e.preventDefault();
            resultsContainer.html('');
            projectsTable.hide();
            clearSearchTrigger.show();
            let noResulsHtml = '<tr>' +
                                '<td>@lang("projectsmanagement.search.no-results")</td>' +
                                '<td></td>' +
                                '<td class="hidden-xs"></td>' +
                                '<td class="hidden-xs"></td>' +
                                '<td class="hidden-xs"></td>' +
                                '<td class="hidden-sm hidden-xs"></td>' +
                                '<td class="hidden-sm hidden-xs hidden-md"></td>' +
                                '<td class="hidden-sm hidden-xs hidden-md"></td>' +
                                '<td></td>' +
                                '<td></td>' +
                                '<td></td>' +
                                '</tr>';

            $.ajax({
                type:'POST',
                url: "{{ route('search-projects') }}",
                data: searchform.serialize(),
                success: function (result) {
                    let jsonData = JSON.parse(result);
                    if (jsonData.length != 0) {
                        $.each(jsonData, function(index, val) {
                            let rolesHtml = '';
                            let roleClass = '';
                            let showCellHtml = '<a class="btn btn-sm btn-success btn-block" href="projects/' + val.id + '" data-toggle="tooltip" title="@lang("projectsmanagement.tooltips.show")">@lang("projectsmanagement.buttons.show")</a>';
                            let editCellHtml = '<a class="btn btn-sm btn-info btn-block" href="projects/' + val.id + '/edit" data-toggle="tooltip" title="@lang("projectsmanagement.tooltips.edit")">@lang("projectsmanagement.buttons.edit")</a>';
                            let deleteCellHtml = '<form method="POST" action="/projects/'+ val.id +'" accept-charset="UTF-8" data-toggle="tooltip" title="Delete">' +
                                    '{!! Form::hidden("_method", "DELETE") !!}' +
                                    '{!! csrf_field() !!}' +
                                    '<button class="btn btn-danger btn-sm" type="button" style="width: 100%;" data-toggle="modal" data-target="#confirmDelete" data-title="Delete Project" data-message="@lang("projectsmanagement.modals.delete_project_message", ["project" => "'+val.name+'"])">' +
                                        '@lang("projectsmanagement.buttons.delete")' +
                                    '</button>' +
                                '</form>';

                            resultsContainer.append('<tr>' +
                                '<td>' + val.id + '</td>' +
                                '<td>' + val.projectname + '</td>' +
                                '<td class="hidden-xs">' + val.type + '</td>' +
                                '<td class="hidden-xs">' + val.location + '</td>' +
                                '<td class="hidden-sm hidden-xs hidden-md">' + val.created_at + '</td>' +
                                '<td class="hidden-sm hidden-xs hidden-md">' + val.updated_at + '</td>' +
                                '<td>' + deleteCellHtml + '</td>' +
                                '<td>' + showCellHtml + '</td>' +
                                '<td>' + editCellHtml + '</td>' +
                            '</tr>');
                        });
                    } else {
                        resultsContainer.append(noResulsHtml);
                    };
                    projectsCount.html(jsonData.length + " @lang('projectsmanagement.search.found-footer')");
                    projectPagination.hide();
                    cardTitle.html("@lang('projectsmanagement.search.title')");
                },
                error: function (response, status, error) {
                    if (response.status === 422) {
                        resultsContainer.append(noResulsHtml);
                        projectsCount.html(0 + " @lang('projectsmanagement.search.found-footer')");
                        projectPagination.hide();
                        cardTitle.html("@lang('projectsmanagement.search.title')");
                    };
                },
            });
        });
        searchSubmit.click(function(event) {
            event.preventDefault();
            searchform.submit();
        });
        searchformInput.keyup(function(event) {
            if ($('#project_search_box').val() != '') {
                clearSearchTrigger.show();
            } else {
                clearSearchTrigger.hide();
                resultsContainer.html('');
                projectsTable.show();
                cardTitle.html("@lang('projectsmanagement.showing-all-projects')");
                projectPagination.show();
                projectsCount.html(" ");
            };
        });
        clearSearchTrigger.click(function(e) {
            e.preventDefault();
            clearSearchTrigger.hide();
            projectsTable.show();
            resultsContainer.html('');
            searchformInput.val('');
            cardTitle.html("@lang('projectsmanagement.showing-all-projects')");
            projectPagination.show();
            projectsCount.html(" ");
        });
    });
</script>
