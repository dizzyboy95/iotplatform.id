<script>
    $(function() {
        var cardTitle = $('#card_title');
        var devicesTable = $('#devices_table');
        var resultsContainer = $('#search_results');
        var devicesCount = $('#device_count');
        var clearSearchTrigger = $('.clear-search');
        var searchform = $('#search_devices');
        var searchformInput = $('#device_search_box');
        var devicePagination = $('#device_pagination');
        var searchSubmit = $('#search_trigger');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        searchform.submit(function(e) {
            e.preventDefault();
            resultsContainer.html('');
            devicesTable.hide();
            clearSearchTrigger.show();
            let noResulsHtml = '<tr>' +
                                '<td>@lang("devicesmanagement.search.no-results")</td>' +
                                '<td></td>' +
                                '<td class="hidden-xs"></td>' +
                                '<td class="hidden-xs"></td>' +
                                '<td class="hidden-xs"></td>' +
                                '<td class="hidden-sm hidden-xs"></td>' +
                                '<td class="hidden-sm hidden-xs hidden-md"></td>' +
                                '<td class="hidden-sm hidden-xs hidden-md"></td>' +
                                '<td></td>' +
                                '<td></td>' +
                                '<td></td>' +
                                '</tr>';

            $.ajax({
                type:'POST',
                url: "{{ route('search-devices') }}",
                data: searchform.serialize(),
                success: function (result) {
                    let jsonData = JSON.parse(result);
                    if (jsonData.length != 0) {
                        $.each(jsonData, function(index, val) {
                            let rolesHtml = '';
                            let roleClass = '';
                            let showCellHtml = '<a class="btn btn-sm btn-success btn-block" href="projects/' . $id + val.id + '" data-toggle="tooltip" title="@lang("devicesmanagement.tooltips.show")">@lang("devicesmanagement.buttons.show")</a>';
                            let deleteCellHtml = '<form method="POST" action="projects/' . $id + val.id +'" accept-charset="UTF-8" data-toggle="tooltip" title="Delete">' +
                                    '{!! Form::hidden("_method", "DELETE") !!}' +
                                    '{!! csrf_field() !!}' +
                                    '<button class="btn btn-danger btn-sm" type="button" style="width: 100%;" data-toggle="modal" data-target="#confirmDelete" data-title="Delete Device" data-message="@lang("devicesmanagement.modals.delete_device_message", ["device" => "'+val.name+'"])">' +
                                        '@lang("devicesmanagement.buttons.delete")' +
                                    '</button>' +
                                '</form>';

                            resultsContainer.append('<tr>' +
                                '<td>' + val.id + '</td>' +
                                '<td>' + val.devicename + '</td>' +
                                '<td>' + deleteCellHtml + '</td>' +
                                '<td>' + showCellHtml + '</td>' +
                            '</tr>');
                        });
                    } else {
                        resultsContainer.append(noResulsHtml);
                    };
                    devicesCount.html(jsonData.length + " @lang('devicesmanagement.search.found-footer')");
                    devicePagination.hide();
                    cardTitle.html("@lang('devicesmanagement.search.title')");
                },
                error: function (response, status, error) {
                    if (response.status === 422) {
                        resultsContainer.append(noResulsHtml);
                        devicesCount.html(0 + " @lang('devicesmanagement.search.found-footer')");
                        devicePagination.hide();
                        cardTitle.html("@lang('devicesmanagement.search.title')");
                    };
                },
            });
        });
        searchSubmit.click(function(event) {
            event.preventDefault();
            searchform.submit();
        });
        searchformInput.keyup(function(event) {
            if ($('#device_search_box').val() != '') {
                clearSearchTrigger.show();
            } else {
                clearSearchTrigger.hide();
                resultsContainer.html('');
                devicesTable.show();
                cardTitle.html("@lang('devicesmanagement.showing-all-devices')");
                devicePagination.show();
                devicesCount.html(" ");
            };
        });
        clearSearchTrigger.click(function(e) {
            e.preventDefault();
            clearSearchTrigger.hide();
            devicesTable.show();
            resultsContainer.html('');
            searchformInput.val('');
            cardTitle.html("@lang('devicesmanagement.showing-all-devices')");
            devicePagination.show();
            devicesCount.html(" ");
        });
    });
</script>
