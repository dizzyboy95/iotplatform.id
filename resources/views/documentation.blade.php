<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
  <title>IotPlatform.id Documentation</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
  <link rel="shortcut icon" type="image/png" href="http://iotplatform.id/images/favicon.png">

  <style>
  body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", sans-serif}
  body, html {
              height: 100%;
              line-height: 1.8;
              }
  /* Full height image header */
  .bgimg-1 {
            background-position: center;
            background-size: cover;
            background-image: url({{url('/images/frontend_images/about-header-background-for-web.jpg')}});
            min-height: 100%;
          }
  .w3-bar .w3-button {
                      padding: 16px;
                      text-decoration-line: none;
                    }
  .w3-button:hover {
                    text-decoration-line: none;
                  }
  a:hover {
    text-decoration-line: none;
  }
  
  </style>
</head>

<body>
  <!-- Navbar (sit on top) -->
  <div class="w3-top">
    <div class="w3-bar w3-white w3-card" id="myNavbar">
      @if (Route::has('login'))
        <a href="{{ url('/') }}" class="w3-bar-item w3-button w3-wide">IoTPlatform.id</a>
          <!-- Right-sided navbar links -->
          <div class="w3-right w3-hide-small">
            @if (Auth::check())
              <a href="{{ url('/home') }}" class="w3-bar-item w3-button"><i class="fa fa-home"></i> HOME</a>
            @else
              <a href="{{ url('/login') }}" class="w3-bar-item w3-button"><i class="fa fa-sign-in"></i> LOGIN</a>
              <a href="{{ url('/register') }}" class="w3-bar-item w3-button"><i class="fa fa-user-plus"></i> REGISTER</a>
              <a href="{{ url('/documentation') }}" class="w3-bar-item w3-button"><i class="fa fa-book"></i> DOCUMENTATION</a>
            @endif
          </div>
      @endif
      <!-- Hide right-floated links on small screens and replace them with a menu icon -->
      <a href="javascript:void(0)" class="w3-bar-item w3-button w3-right w3-hide-large w3-hide-medium" onclick="w3_open()">
          <i class="fa fa-bars"></i>
      </a>
    </div>
  </div>

  <!-- Sidebar on small screens when clicking the menu icon -->
  <nav class="w3-sidebar w3-bar-block w3-black w3-card w3-animate-left w3-hide-medium w3-hide-large" style="display:none" id="mySidebar">
    <a href="javascript:void(0)" onclick="w3_close()" class="w3-bar-item w3-button w3-large w3-padding-16">Close ×</a>
    <a href="{{ url('/login') }}" onclick="w3_close()" class="w3-bar-item w3-button">LOGIN</a>
    <a href="{{ url('/register') }}" onclick="w3_close()" class="w3-bar-item w3-button">REGISTER</a>
    <a href="{{ url('/documentation') }}" onclick="w3_close()" class="w3-bar-item w3-button">DOCUMENTATION</a>
  </nav>

  <!-- Header with full-height image -->
  <header class="bgimg-1 w3-display-container w3-grayscale-min" id="home">
    <div class="w3-display-middle w3-text-white" style="text-align: center;">
      <span class="w3-jumbo w3-hide-small"><strong>Documentation</strong></span><br>
      <span class="w3-xxlarge w3-hide-large w3-hide-medium"><strong>Documentation</strong></span><br>
      <span class="w3-large"><strong>This documentation contains technical details, useful tool, API HTTP, etc. of this IoTPlatform.id</strong></span>
    </div> 
  </header>

  <!-- Page content -->
  <div class="w3-content" style="max-width:1100px">

    <!-- Getting Started Section -->
    <div class="w3-row w3-padding-64" id="about">
      <h1 class="w3-center"> <b>Getting Started</b> </h1><br>
      <p class="w3-large" align="justify">PATRIOT is a Horizontal IoT Platform. What does it mean ? We are trying to be as generic as possible so that your IoT Vertical Solution would be fit with the global architecture. There is a lot of possible use cases would fit with us, for example, smart home, smart metering, asset tracking, smart building, etc.</p>
      <p class="w3-large" align="justify">This tutorial will show you how to store data of your sensor to PATRIOT. Hence, there are 4 steps you have to follow:</p>
        <ol type="1">
          <li>Register for an account</li>
          <li>Create a project</li>
          <li>Add your device</li>
          <li>Store data of your device</li>
        </ol>
      <p class="w3-large" align="justify">The detail steps found below. If you think there is a wrong information or not working regarding this tutorial, please do not hesitate to mail us at <a href="mailto:tugas1akhir1@gmail.com" style="color: #0facf3;">tugas1akhir1@gmail.com</a></p>
    </div>
  
    <hr>
  
    <!-- Register Account Section -->
    <div class="w3-row w3-padding-64" id="about">
      <div class="w3-col l6 w3-padding-large">
        <h1 class="w3-center"> <b>Register for an Account</b> </h1><br>
        <p class="w3-large" align="justify"> Wanna register ? Please find an account by visiting <a href="{{ url('/register') }}" style="color: #0facf3;" target="_blank"> http://159.89.199.182/register </a> Or Clicking <b>"Register" </b> in the Register page.</p>   
      </div>
      <div class="w3-col l6 w3-padding-large">
        <img src="{{ url('/images/frontend_images/registerpage.jpg') }}" class="w3-round w3-image w3-opacity-min" style="width:100%;">
        <p align="center">Figure 1. User Interface of Register Page</p>
      </div>
    </div>

    <hr>

    <!-- Login Account Section -->
    <div class="w3-row w3-padding-64" id="about">
      <div class="w3-col m6 w3-padding-large w3-hide-small">
        <img src="{{ url('/images/frontend_images/loginpage.jpg') }}" class="w3-round w3-image w3-opacity-min" style="width: 100%;">
        <p align="center">Figure 2. User Interface of Login Page</p>
      </div>
      <div class="w3-col m6 w3-padding-large">
        <h1 class="w3-center"> <b>Login for Your Account</b> </h1><br>
        <p class="w3-large" align="justify"> After register account, our login page can be found at <a href="{{ url('/login') }}" style="color: #0facf3;" target="_blank">http://159.89.199.182/login.</a> If you already have an account, just login through that page. Otherwise you have to register an account in order to use PATRIOT services.</p>
      </div>
    </div>

    <hr>

    <!-- Verification Account Section -->
    <div class="w3-row w3-padding-64" id="about">
      <div class="w3-col l6 w3-padding-large">
        <h1 class="w3-center"> <b>Verification for Your Account</b> </h1><br>
        <p class="w3-large" align="justify"> Once you finished filling all the informations. We will send you an email verification. Please click it to verify your account. <b>If your verification email did not come</b>, you can click <b>“Click here to resend the email”</b> as depicted in figure below.</p>   
      </div>
      <div class="w3-col l6 w3-padding-large">
        <img src="{{ url('/images/frontend_images/afterregister.jpg') }}" class="w3-round w3-image w3-opacity-min" style="width:100%;">
        <p align="center">Figure 3. User Interface of Verification Email Page</p>
      </div>
    </div>

    <hr>

    <!-- After Login Account Section -->
    <div class="w3-row w3-padding-64" id="about">
      <div class="w3-col m6 w3-padding-large w3-hide-small">
        <img src="{{ url('/images/frontend_images/afterlogin.jpg') }}" class="w3-round w3-image w3-opacity-min" style="width: 100%;">
        <p align="center">Figure 4. User Interface of User's Page</p>
      </div>
      <div class="w3-col m6 w3-padding-large">
        <h1 class="w3-center"> <b>User Page</b> </h1><br>
        <p class="w3-large" align="justify"> Since you already have your account then you can go to the User's page as below.</p>
      </div>
    </div>

    <hr> 

    <!-- Get Key Access Account Section -->
    <div class="w3-row w3-padding-64" id="about">
      <div class="w3-col l6 w3-padding-large">
        <h1 class="w3-center"> <b>Get Access Key for Your Account</b> </h1><br>
        <p class="w3-large" align="justify"> Prior to creating a project, you have to <b>generate an access key</b>. This process only happens once when you are a new member. You can find the menu in the <b>Account -> Access Key</b>. Access key determines your account, please keep it safe.</p>   
      </div>
      <div class="w3-col l6 w3-padding-large">
        <img src="{{ url('/images/frontend_images/accountpage.png') }}" class="w3-round w3-image w3-opacity-min" style="width:100%;">
        <p align="center">Figure 5. User Interface of Access Key from Account Page</p>
      </div>
    </div>

    <hr>

    <!-- Create Project Section -->
    <div class="w3-row w3-padding-64" id="about">
      <div class="w3-col m6 w3-padding-large w3-hide-small">
        <img src="{{ url('/images/frontend_images/exampleproject.jpg') }}" class="w3-round w3-image w3-opacity-min" style="width: 100%;">
        <p align="center">Figure 6. User Interface of Create a Project Page</p>
      </div>
      <div class="w3-col m6 w3-padding-large">
        <h1 class="w3-center"> <b>Create a Project</b> </h1><br>
        <p class="w3-large" align="justify"> Then, we can start to create a project. Please go back to <b>Project</b>. In the dashboard page, click <b>“Create a Project”</b>. This will forward you to this page.</p>
      </div>
    </div>

    <hr>

    <!-- Project Created Section -->
    <div class="w3-row w3-padding-64" id="about">
      <div class="w3-col l6 w3-padding-large">
        <h1 class="w3-center"> <b>Project Created</b> </h1><br>
        <p class="w3-large" align="justify"> <b>Voilaa!! Congratulation you just made your first project</b>. You can find it in your dashboard.</p>   
      </div>
      <div class="w3-col l6 w3-padding-large">
        <img src="{{ url('/images/frontend_images/projectcreated.jpg') }}" class="w3-round w3-image w3-opacity-min" style="width:100%;">
        <p align="center">Figure 7. User Interface of Project Created</p>
      </div>
    </div>

    <hr>

    <!-- Add Device Section -->
    <div class="w3-row w3-padding-64" id="about">
      <h1 class="w3-center"> <b>Add Device under Your Project</b> </h1><br>
      <p class="w3-large" align="justify"> Internet of Things is about Things. This step is dedicated to teach on how to create the things which we called “device” in our environment.</p>
      <div class="w3-col m6 w3-padding-large w3-hide-small">
        <img src="{{ url('/images/frontend_images/adddevice.jpg') }}" class="w3-round w3-image w3-opacity-min" style="width:100%;">
        <p align="center">Figure 8. User Interface of Add Device Page</p>
      </div>
      <div class="w3-col m6 w3-padding-large">
        <p class="w3-large" align="justify"> The UI below will appear, you just click the <b>“Add Device”</b>. You can also create the devices through the RESTFUL API. Specifically in Section <b>HTTP API</b>. You can also subscribe your device so later when there is any changes or new data coming, your monitor will be <b>notified</b>. You can utilize the information to create your own logic.</p>
      </div>
    </div>

    <hr>

    <!-- Pick Device Name Section -->
    <div class="w3-row w3-padding-64" id="about">
      <div class="w3-col l6 w3-padding-large">
        <h1 class="w3-center"> <b>Add Device Name</b> </h1><br>
        <p class="w3-large" align="justify"> Pick a name for your device, in our case, we choose <b>"Sensor Cuaca"</b>.</p>   
      </div>
      <div class="w3-col l6 w3-padding-large">
        <img src="{{ url('/images/frontend_images/pickdevicename.jpg') }}" class="w3-round w3-image w3-opacity-min" style="width:100%;">
        <p align="center">Figure 9. User Interface of Add Device Name</p>
      </div>
    </div>

    <hr>

    <!-- Device Created Section -->
    <div class="w3-row w3-padding-64" id="about">
      <div class="w3-col m6 w3-padding-large w3-hide-small">
        <img src="{{ url('/images/frontend_images/devicecreated.jpg') }}" class="w3-round w3-image w3-opacity-min" style="width: 100%;">
        <p align="center">Figure 10. User Interface of Device Added</p>
      </div>
      <div class="w3-col m6 w3-padding-large">
        <h1 class="w3-center"> <b>Device Added</b> </h1><br>
        <p class="w3-large" align="justify"> Great you just made your first device!! Now let’s start to store some data into it.</p>
      </div>
    </div>

    <hr>

    <!-- API HTTP Section -->
    <div class="w3-row w3-padding-64" id="about">
      <h1 class="w3-center"> <b>API HTTP</b> </h1>
      <h2 class="w3-center">Device</h2>
      <h4> <b>Create</b> </h4>
      <table class="table table-bordered data-table">
        <thead>
          <tr>
            <th><b>Field</b></th>
            <th colspan="2" style="text-align: center;"><b>Value</b></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>URL</td>
            <td colspan="2"><code>skripsweet.tk/api/v1/device</code></td>
          </tr>
          <tr>
            <td>Method</td>
            <td colspan="2"><code>POST</code></td>
          </tr>
          <tr>
            <td rowspan="4">Header</td>
            <td style="text-align: center;"><b>Key</b></td>
            <td style="text-align: center;"><b>Value</b></td>
          </tr>
          <tr>
            <td>Authorization</td>
            <td>Bearer [spasi]<code> Access_Key</code></td>
          </tr>
          <tr>
            <td>Content-Type</td>
            <td><code>application/json</code></td>
          </tr>
          <tr>
            <td>Accept</td>
            <td><code>application/json</code></td>
          </tr>
          <tr>
            <td>Body</td>
            <td colspan="2">
              <pre>
                <span><code>{</code></span>
                <span><code>"devicename" : "your device name",</code></span>
                <span><code>"project_id" : "your project_id"</code></span>
                <span><code>}</code></span>
              </pre>
            </td>
          </tr>
        </tbody>
      </table>
      <hr>
      <h4> <b>Retrieve All Device ID of a Particular Project</b> </h4>
      <table class="table table-bordered data-table">
        <thead>
          <tr>
            <th><b>Field</b></th>
            <th colspan="2" style="text-align: center;"><b>Value</b></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>URL</td>
            <td colspan="2"><code>skripsweet.tk/api/v1/device</code></td>
          </tr>
          <tr>
            <td>Method</td>
            <td colspan="2"><code>GET</code></td>
          </tr>
          <tr>
            <td rowspan="4">Header</td>
            <td style="text-align: center;"><b>Key</b></td>
            <td style="text-align: center;"><b>Value</b></td>
          </tr>
          <tr>
            <td>Authorization</td>
            <td>Bearer [spasi]<code> Access_Key</code></td>
          </tr>
          <tr>
            <td>Content-Type</td>
            <td><code>application/json</code></td>
          </tr>
          <tr>
            <td>Accept</td>
            <td><code>application/json</code></td>
          </tr>
        </tbody>
      </table>
      <hr>
      <h4> <b>Retrieve a Particular Device</b> </h4>
      <table class="table table-bordered data-table">
        <thead>
          <tr>
            <th><b>Field</b></th>
            <th colspan="2" style="text-align: center;"><b>Value</b></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>URL</td>
            <td colspan="2"><code>skripsweet.tk/api/v1/device/id_device</code></td>
          </tr>
          <tr>
            <td>Method</td>
            <td colspan="2"><code>GET</code></td>
          </tr>
          <tr>
            <td rowspan="4">Header</td>
            <td style="text-align: center;"><b>Key</b></td>
            <td style="text-align: center;"><b>Value</b></td>
          </tr>
          <tr>
            <td>Authorization</td>
            <td>Bearer [spasi]<code> Access_Key</code></td>
          </tr>
          <tr>
            <td>Content-Type</td>
            <td><code>application/json</code></td>
          </tr>
          <tr>
            <td>Accept</td>
            <td><code>application/json</code></td>
          </tr>
        </tbody>
      </table>
      <hr>
      <h4> <b>Delete a Particular Device</b> </h4>
      <table class="table table-bordered data-table">
        <thead>
          <tr>
            <th><b>Field</b></th>
            <th colspan="2" style="text-align: center;"><b>Value</b></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>URL</td>
            <td colspan="2"><code>skripsweet.tk/api/v1/device/id_device</code></td>
          </tr>
          <tr>
            <td>Method</td>
            <td colspan="2"><code>DELETE</code></td>
          </tr>
          <tr>
            <td rowspan="4">Header</td>
            <td style="text-align: center;"><b>Key</b></td>
            <td style="text-align: center;"><b>Value</b></td>
          </tr>
          <tr>
            <td>Authorization</td>
            <td>Bearer [spasi]<code> Access_Key</code></td>
          </tr>
          <tr>
            <td>Content-Type</td>
            <td><code>application/json</code></td>
          </tr>
          <tr>
            <td>Accept</td>
            <td><code>application/json</code></td>
          </tr>
        </tbody>
      </table>
      <hr>
      <h2 class="w3-center"> Data of Device</h2>
      <h4> <b>Store Data of a Particular Device</b> </h4>
      <table class="table table-bordered data-table">
        <thead>
          <tr>
            <th><b>Field</b></th>
            <th colspan="2" style="text-align: center;"><b>Value</b></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>URL</td>
            <td colspan="2"><code>skripsweet.tk/api/v1/status</code></td>
          </tr>
          <tr>
            <td>Method</td>
            <td colspan="2"><code>POST</code></td>
          </tr>
          <tr>
            <td rowspan="4">Header</td>
            <td style="text-align: center;"><b>Key</b></td>
            <td style="text-align: center;"><b>Value</b></td>
          </tr>
          <tr>
            <td>Authorization</td>
            <td>Bearer [spasi]<code> Access_Key</code></td>
          </tr>
          <tr>
            <td>Content-Type</td>
            <td><code>application/json</code></td>
          </tr>
          <tr>
            <td>Accept</td>
            <td><code>application/json</code></td>
          </tr>
          <tr>
            <td>Body</td>
            <td colspan="2">
              <pre>
                <span><code>{</code></span>
                <span><code>"device_id" : "your device_id",</code></span>
                <span><code>"nilai" : "value",</code></span>
                <span><code>"satuan" : "key"</code></span>
                <span><code>}</code></span>
                <br>
                <span>Note : </span>
                <span>nilai ex:80,90,100,etc</span>
                <span>satuan ex:meter,celcius,lux,etc</span>
              </pre>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
   
    <hr>

    <!-- Useful Tool Section -->
    <div class="w3-row w3-padding-64 w3-center" id="about">
      <h1 class="w3-center"> Useful Tool </h1><br>
      <p class="w3-large w3-center" align="justify"> Here are the useful tool you can use to debug and develop your applications.</p>
      <img src="https://www.getpostman.com/img/v2/logo-glyph.png" class="w3-round w3-image w3-opacity-min " style="width:20%;">
        <h1 class="w3-center">Postman</h1><br>
        <p class="w3-large" align="justify"> Postman is a tool that acts as a RESTful HTTP(s) client. Before you deploy of either posting or retrieving your data, you can use Postman to emulate the function. Once it is okay, then no doubtly it could work on your device. It can also help you to generate the code that suit with your programming language.</p>
        <p class="w3-large" align="justify"><a href="https://www.getpostman.com/postman" style="color: #0facf3;" target="_blank">Go To Postman Website</a></p>   
    </div>    
  <!-- End page content -->
  </div>
</body>
  <!-- Footer -->
  <footer class="w3-center w3-black w3-padding-64">
    <a href="{{ url('/documentation') }}" class="w3-button w3-light-grey"><i class="fa fa-arrow-up w3-margin-right"></i>To the top</a>
    <div class="w3-xlarge w3-section">
      <i class="fa fa-facebook-official w3-hover-opacity"></i>
      <i class="fa fa-instagram w3-hover-opacity"></i>
      <i class="fa fa-snapchat w3-hover-opacity"></i>
      <i class="fa fa-pinterest-p w3-hover-opacity"></i>
      <i class="fa fa-twitter w3-hover-opacity"></i>
      <i class="fa fa-linkedin w3-hover-opacity"></i>
    </div>
    <p>Powered by <a href="https://see.telkomuniversity.ac.id/" title="W3.CSS" target="_blank" class="w3-hover-text-green">FTE Telkom University</a></p>
  </footer>
</html>