@extends('layouts.app4')

@section('template_title')
@endsection

@section('template_linked_css')
@endsection

@section('content')
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="fa fa-home"></i> Home</a> > <a href="#" class="current"><i class="fa fa-user-circle"></i> Account</a> </div>
  </div>
  <div class="container-fluid">
    <hr>
    @include('partials.form-status')
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="fa fa-user"></i> </span>
            <h5>Edit Account {{ Auth::user()->name }}</h5>
          </div>  

          <div class="widget-content nopadding">
          
            {!! Form::model($user, array('action' => array('AccountManagementController@updateUserAccount', $user->id), 'method' => 'PUT', 'id' => 'user_basics_form', 'class' => 'form-horizontal')) !!}

              {!! csrf_field() !!}

              <div class="form-group has-feedback row {{ $errors->has('name') ? ' has-error ' : '' }}">
                {!! Form::label('name', 'Username' , array('class' => 'col-md-3 control-label')); !!}
                  <div class="controls">
                    <div class="input-group">
                      {!! Form::text('name', old('name'), array('id' => 'name', 'class' => 'form-control', 'placeholder' => trans('forms.ph-username'))) !!} <i class="fa fa-fw fa-user" aria-hidden="true"></i>
                    </div>
                  </div>
              </div>
              <div class="form-group has-feedback row {{ $errors->has('email') ? ' has-error ' : '' }}">
                  {!! Form::label('email', 'E-mail' , array('class' => 'col-md-3 control-label')); !!}
                    <div class="controls">
                      <div class="input-group">
                        {!! Form::text('email', old('email'), array('id' => 'email', 'class' => 'form-control', 'disabled' => 'disabled', 'placeholder' => trans('forms.ph-useremail'))) !!} <i class="fa fa-fw fa-envelope" aria-hidden="true"></i>
                      </div>
                    </div>
              </div>
              <div class="form-group has-feedback row {{ $errors->has('first_name') ? ' has-error ' : '' }}">
                  {!! Form::label('first_name', trans('forms.create_user_label_firstname'), array('class' => 'col-md-3 control-label')); !!}
                    <div class="controls">
                      <div class="input-group">
                        {!! Form::text('first_name', NULL, array('id' => 'first_name', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_firstname'))) !!} <i class="fa fa-fw {{ trans('forms.create_user_icon_firstname') }}" aria-hidden="true"></i>
                      </div>
                        @if ($errors->has('first_name'))
                          <span class="help-block">
                              <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                        @endif
                    </div>
              </div>
              <div class="form-group has-feedback row {{ $errors->has('last_name') ? ' has-error ' : '' }}">
                  {!! Form::label('last_name', trans('forms.create_user_label_lastname'), array('class' => 'col-md-3 control-label')); !!}
                    <div class="controls">
                      <div class="input-group margin-bottom-1">
                        {!! Form::text('last_name', NULL, array('id' => 'last_name', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_lastname'))) !!} <i class="fa fa-fw {{ trans('forms.create_user_icon_lastname') }}" aria-hidden="true"></i>
                      </div>
                        @if ($errors->has('last_name'))
                          <span class="help-block">
                            <strong>{{ $errors->first('last_name') }}</strong>
                          </span>
                        @endif
                    </div>
              </div>
              <div class="form-group has-feedback row {{ $errors->has('api_token') ? ' has-error ' : '' }}">
                  {!! Form::label('api_token', 'Access Key' , array('class' => 'col-md-3 control-label')); !!}
                    <div class="controls">
                      <div class="input-group">
                        {!! Form::text('api_token', old('api_token'), array('id' => 'api_token', 'class' => 'form-control', 'disabled' => 'disabled', 'placeholder' => trans('forms.ph-api_token'))) !!} <i class="fa fa-fw fa-key" aria-hidden="true"></i> <a onclick="myFunction()" href="#" title="Copy Access Key" class="tip-bottom"><i class="fa fa-fw fa-clone" aria-hidden="true"></i></a>
                      </div>
                    </div>
              </div>

              {!! Form::model($user, array('action' => array('AccountManagementController@updateUserPassword', $user->id), 'method' => 'PUT', 'autocomplete' => 'new-password', 'class' => 'form-horizontal')) !!}

                <div class="pw-change-container margin-bottom-2">
                  <div class="form-group has-feedback row {{ $errors->has('password') ? ' has-error ' : '' }}">
                    {!! Form::label('password', trans('forms.create_user_label_password'), array('class' => 'col-md-3 control-label')); !!}
                      <div class="controls">
                        {!! Form::password('password', array('id' => 'password', 'class' => 'form-control ', 'placeholder' => trans('forms.create_user_ph_password'), 'autocomplete' => 'new-password')) !!}
                          @if ($errors->has('password'))
                            <span class="help-block">
                              <strong>{{ $errors->first('password') }}</strong>
                            </span>
                          @endif
                      </div>
                  </div>
                  <div class="form-group has-feedback row {{ $errors->has('password_confirmation') ? ' has-error ' : '' }}">
                    {!! Form::label('password_confirmation', trans('forms.create_user_label_pw_confirmation'), array('class' => 'col-md-3 control-label')); !!}
                      <div class="controls">
                        {!! Form::password('password_confirmation', array('id' => 'password_confirmation', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_pw_confirmation'))) !!}
                          <span id="pw_status"></span>
                            @if ($errors->has('password_confirmation'))
                              <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                              </span>
                            @endif
                      </div>
                  </div>
                </div>
                      
              <div class="form-actions">
                {!! Form::button(
                  '<i class="fa fa-fw fa-save" aria-hidden="true"></i> ' . trans('profile.submitPWButton'),
                    array(
                          'class'             => 'btn btn-warning',
                          'id'                => 'pw_save_trigger',
                          'disabled'          => true,
                          'type'              => 'button',
                          'data-submit'       => trans('profile.submitButton'),
                          'data-target'       => '#confirmForm',
                          'data-modalClass'   => 'modal-warning',
                          'data-toggle'       => 'modal',
                          'data-title'        => trans('modals.edit_user__modal_text_confirm_title'),
                          'data-message'      => trans('modals.edit_user__modal_text_confirm_message')
                        )) !!}
                  
                <div class="pull-right">
                  {!! Form::button(
                    '<i class="fa fa-fw fa-save" aria-hidden="true"></i> ' . trans('profile.submitProfileButton'),
                        array(
                              'class'           => 'btn btn-success',
                              'id'              => 'account_save_trigger',
                              'disabled'        => true,
                              'type'            => 'button',
                              'data-submit'     => trans('profile.submitProfileButton'),
                              'data-target'     => '#confirmForm',
                              'data-modalClass' => 'modal-success',
                              'data-toggle'     => 'modal',
                              'data-title'      => trans('modals.edit_user__modal_text_confirm_title'),
                              'data-message'    => trans('modals.edit_user__modal_text_confirm_message')
                            )) !!}
                              
                </div>
              </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

  @include('modals.modal-form')

@endsection

@section('footer_scripts')

  @include('scripts.form-modal-script')

  <script type="text/javascript">

    $('.dropdown-menu li a').click(function() {
      $('.dropdown-menu li').removeClass('active');
    });

    $('.profile-trigger').click(function() {
      $('.panel').alterClass('panel-*', 'panel-default');
    });

    $('.settings-trigger').click(function() {
      $('.panel').alterClass('panel-*', 'panel-info');
    });

    $('.admin-trigger').click(function() {
      $('.panel').alterClass('panel-*', 'panel-warning');
      $('.edit_account .nav-pills li, .edit_account .tab-pane').removeClass('active');
      $('#changepw')
        .addClass('active')
        .addClass('in');
      $('.change-pw').addClass('active');
    });

    $('.warning-pill-trigger').click(function() {
      $('.panel').alterClass('panel-*', 'panel-warning');
    });

    $('.danger-pill-trigger').click(function() {
      $('.panel').alterClass('panel-*', 'panel-danger');
    });

    $('#user_basics_form').on('keyup change', 'input, select, textarea', function(){
        $('#account_save_trigger').attr('disabled', false);
    });

    $('#checkConfirmDelete').change(function() {
        var submitDelete = $('#delete_account_trigger');
        var self = $(this);

        if (self.is(':checked')) {
            submitDelete.attr('disabled', false);
        }
        else {
          submitDelete.attr('disabled', true);
        }
    });

    $("#password_confirmation").keyup(function() {
      checkPasswordMatch();
    });

    $("#password, #password_confirmation").keyup(function() {
      enableSubmitPWCheck();
    });

    $('#password, #password_confirmation').hidePassword(true);

    $('#password').password({
      shortPass: 'The password is too short',
      badPass: 'Weak - Try combining letters & numbers',
      goodPass: 'Medium - Try using special charecters',
      strongPass: 'Strong password',
      containsUsername: 'The password contains the username',
      enterPass: false,
      showPercent: false,
      showText: true,
      animate: true,
      animateSpeed: 50,
      username: false, // select the username field (selector or jQuery instance) for better password checks
      usernamePartialMatch: true,
      minimumLength: 6
    });

    function checkPasswordMatch() {
        var password = $("#password").val();
        var confirmPassword = $("#password_confirmation").val();
        if (password != confirmPassword) {
            $("#pw_status").html("Passwords do not match!");
        }
        else {
            $("#pw_status").html("Passwords match.");
        }
    }

    function enableSubmitPWCheck() {
        var password = $("#password").val();
        var confirmPassword = $("#password_confirmation").val();
        var submitChange = $('#pw_save_trigger');
        if (password != confirmPassword) {
            submitChange.attr('disabled', true);
        }
        else {
            submitChange.attr('disabled', false);
        }
    }

    function myFunction() {
        /* Get the text field */
        var copyText = document.getElementById("api_token");

        /* Select the text field */
        copyText.select();

        /* Copy the text inside the text field */
        document.execCommand("copy");

        /* Alert the copied text */
        alert("Now copy the access key : " + copyText.value);
    } 

  </script>

@endsection