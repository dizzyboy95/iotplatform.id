<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="shortcut icon" type="image/png" href="http://iotplatform.id/images/favicon.png">
        <title>IoTPlatform.id </title>
        
        <style>
        body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", sans-serif}
        body, html {
                    height: 100%;
                    line-height: 1.8;
                    }
        /* Full height image header */
        .w3-large {
            color: #5a4646;
        }
        .w3-jumbo {
            color: #5a4646;
        }
        .bgimg-1 {
                    background-position: center;
                    background-size: cover;
                    background-image: url({{url('/images/backend_images/header.jpg')}});
                    min-height: 100%;
                }
        .w3-bar .w3-button {
                            padding: 16px;
                            text-decoration-line: none;
                            }
        .w3-button:hover {
            text-decoration-line: none;
        }
        </style>        
    </head>
    <body>
        <!-- Navbar (sit on top) -->
        <div class="w3-top">
            <div class="w3-bar w3-white w3-card" id="myNavbar">
                @if (Route::has('login'))
                <a href="{{ url('/') }}" class="w3-bar-item w3-button w3-wide">IoT Platform</a>
                    <!-- Right-sided navbar links -->
                    <div class="w3-right w3-hide-small">
                        @if (Auth::check())
                            <a href="{{ url('/home') }}" class="w3-bar-item w3-button"><i class="fa fa-home"></i>HOME</a>
                        @else
                            <a href="{{ url('/login') }}" class="w3-bar-item w3-button"><i class="fa fa-sign-in"></i> LOGIN</a>
                            <a href="{{ url('/register') }}" class="w3-bar-item w3-button"><i class="fa fa-user-plus"></i> REGISTER</a>
                            <a href="{{ url('/documentation') }}" class="w3-bar-item w3-button"><i class="fa fa-book"></i> DOCUMENTATION</a>
                        @endif
                    </div>
                @endif
                <!-- Hide right-floated links on small screens and replace them with a menu icon -->
                <a href="javascript:void(0)" class="w3-bar-item w3-button w3-right w3-hide-large w3-hide-medium" onclick="w3_open()">
                    <i class="fa fa-bars"></i>
                </a>
            </div>
        </div>

        <!-- Sidebar on small screens when clicking the menu icon -->
        <nav class="w3-sidebar w3-bar-block w3-black w3-card w3-animate-left w3-hide-medium w3-hide-large" style="display:none" id="mySidebar">
            <a href="javascript:void(0)" onclick="w3_close()" class="w3-bar-item w3-button w3-large w3-padding-16">Close ×</a>
            <a href="{{ url('/login') }}" onclick="w3_close()" class="w3-bar-item w3-button">LOGIN</a>
            <a href="{{ url('/register') }}" onclick="w3_close()" class="w3-bar-item w3-button">REGISTER</a>
            <a href="{{ url('/documentation') }}" onclick="w3_close()" class="w3-bar-item w3-button">DOCUMENTATION</a>
        </nav>

        <!-- Header with full-height image -->
        <header class="bgimg-1 w3-display-container w3-grayscale-min" id="home">
            <div class="w3-display-left w3-text-white" style="padding:48px">
                <span class="w3-jumbo w3-hide-small">Build for your Everything about IoT</span><br>
                <span class="w3-xxlarge w3-hide-large w3-hide-medium">Build for your Everything about IoT</span><br>
                <span class="w3-large">Stop wasting valuable time with projects that just isn't you.</span>
                <p><a href="{{ url('/register') }}" class="w3-button w3-white w3-padding-large w3-large w3-margin-top w3-opacity w3-hover-opacity-off">Learn more and start today</a></p>
            </div> 
            <div class="w3-display-bottomleft w3-text-grey w3-large" style="padding:24px 48px">
                <i class="fa fa-facebook-official w3-hover-opacity"></i>
                <i class="fa fa-instagram w3-hover-opacity"></i>
                <i class="fa fa-snapchat w3-hover-opacity"></i>
                <i class="fa fa-pinterest-p w3-hover-opacity"></i>
                <i class="fa fa-twitter w3-hover-opacity"></i>
                <i class="fa fa-linkedin w3-hover-opacity"></i>
            </div>
        </header>

        <!-- Contact Section -->
        <!-- <div class="w3-container w3-light-grey" style="padding:128px 16px" id="contact">
            <h3 class="w3-center">CONTACT</h3>
            <p class="w3-center w3-large">Lets get in touch. Send us a message:</p>
            <div class="w3-row-padding" style="margin-top:64px">
                <div class="w3-half">
                    <p><i class="fa fa-graduation-cap fa-fw w3-xxlarge w3-margin-right"></i> Fakultas Teknik Elektro</p>
                    <p><i class="fa fa-building fa-fw w3-xxlarge w3-margin-right"></i> Gedung Deli</p>
                    <p><i class="fa fa-map-marker fa-fw w3-xxlarge w3-margin-right"></i> 
                    Jl. Telekomunikasi No.1, Sukapura, Dayeuhkolot, Bandung, Jawa Barat 40257</p>
                    <p><i class="fa fa-phone fa-fw w3-xxlarge w3-margin-right"></i> +62(22) 7564 108</p>
                    <p><i class="fa fa-envelope fa-fw w3-xxlarge w3-margin-right"> </i> see@telkomuniversity.ac.id</p>
                    <br>
                    <form action="/action_page.php" target="_blank">
                        <p><input class="w3-input w3-border" type="text" placeholder="Name" required name="Name"></p>
                        <p><input class="w3-input w3-border" type="text" placeholder="Email" required name="Email"></p>
                        <p><input class="w3-input w3-border" type="text" placeholder="Subject" required name="Subject"></p>
                        <p><input class="w3-input w3-border" type="text" placeholder="Message" required name="Message"></p>
                        <p>
                            <button class="w3-button w3-black" type="submit">
                                <i class="fa fa-paper-plane"></i> SEND MESSAGE
                            </button>
                        </p>
                    </form>
                </div>
                <div class="w3-half">
                    <!-- Add Google Maps -->
                    <!-- <div id="googleMap" style="width:100%;height:510px;"></div>
                </div>
            </div>
        </div> -->

        <!-- Footer -->
        <footer class="w3-center w3-black w3-padding-64">
            <a href="{{ url('/') }}" class="w3-button w3-light-grey"><i class="fa fa-arrow-up w3-margin-right"></i>To the top</a>
            <div class="w3-xlarge w3-section">
                <i class="fa fa-facebook-official w3-hover-opacity"></i>
                <i class="fa fa-instagram w3-hover-opacity"></i>
                <i class="fa fa-snapchat w3-hover-opacity"></i>
                <i class="fa fa-pinterest-p w3-hover-opacity"></i>
                <i class="fa fa-twitter w3-hover-opacity"></i>
                <i class="fa fa-linkedin w3-hover-opacity"></i>
            </div>
            <p>Credits <a href="http://iotplatform.id/" title="W3.CSS" target="_blank" class="w3-hover-text-green">IoTPlatform.id</a></p>
        </footer>
 
        <!-- Add Google Maps -->
        <script>
            function myMap()
            {
                myCenter=new google.maps.LatLng(-6.975511, 107.629576);
                var mapOptions= {
                            center:myCenter,
                            zoom:17, scrollwheel: false, draggable: false,
                            mapTypeId:google.maps.MapTypeId.ROADMAP
                            };
                var map=new google.maps.Map(document.getElementById("googleMap"),mapOptions);

                var marker = new google.maps.Marker({
                                                position: myCenter,
                                                });
                    marker.setMap(map);
            }

            // Toggle between showing and hiding the sidebar when clicking the menu icon
                var mySidebar = document.getElementById("mySidebar");

            function w3_open() 
            {
                if (mySidebar.style.display === 'block') 
                {
                    mySidebar.style.display = 'none';
                } else 
                {
                    mySidebar.style.display = 'block';
                }
            }

            // Close the sidebar with the close button
            function w3_close() 
            {
                mySidebar.style.display = "none";
            }
        </script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD3QnNJAi-xVnqZh-v-MSrn_gPHcq2hbAA&callback=myMap"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
    </body>
</html>
