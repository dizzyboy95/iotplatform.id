@extends('layouts.app3')

@section('template_title')
  Editing Project {{ $project->projectname }}
@endsection

@section('template_linked_css')
  <style type="text/css">
    .btn-save,
    .pw-change-container {
      display: none;
    }
  </style>
@endsection

@section('content')
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/home') }}" title="Go to Home" class="tip-bottom"><i class="fa fa-home"></i> Home</a> > <a href="{{ url('/projects') }}" title="Go to Create New Projects" class="tip-bottom"><i class="fa fa-database"></i> Projects</a> > <a href="#" class="current"><i class="fa fa-gears"></i> Edit My Projects</a></div>
  </div>
  <div class="container-fluid">
    <hr>
    @include('partials.form-status')
    <div class="row-fluid" style="margin-bottom: 330px; margin-left: 220px;">
      <div class="span7">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="fa fa-cogs"></i> </span>
            <h5>Edit My {{ $project->projectname }} Project</h5>
            <div class="pull-right">
              <a href="{{ URL::to('projects/' . $project->id) }}" style="margin-left: 1em;">
                <span class="label label-warning"><i class="fa fa-fw fa-mail-forward" aria-hidden="true"></i> Go to Device</span>
              </a>
              <a href="{{ url('/projects') }}">
                <span class="label label-info"><i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i> Back to Projects</span>
              </a>
            </div>
          </div>
          <div class="widget-content nopadding">
    
            {!! Form::model($project, array('action' => array('ProjectsManagementController@update', $project->id), 'method' => 'PUT', 'class' => 'form-horizontal')) !!}

              {!! csrf_field() !!}

              <div class="form-group has-feedback row {{ $errors->has('type') ? ' has-error ' : '' }}">
                {!! Form::label('type', 'Type' , array('class' => 'col-md-3 control-label')); !!}
                <div class="controls">
                  <div class="input-group">
                    {!! Form::text('type', old('type'), array('id' => 'type', 'class' => 'form-control', 'placeholder' => trans('forms.ph-projecttype'))) !!} <i class="fa fa-fw fa-tags" aria-hidden="true"></i>
                  </div>
                </div>
              </div>

              <div class="form-group has-feedback row {{ $errors->has('location') ? ' has-error ' : '' }}">
                {!! Form::label('location', trans('forms.create_project_label_location'), array('class' => 'col-md-3 control-label')); !!}
                <div class="controls">
                  <div class="input-group">
                    {!! Form::text('location', NULL, array('id' => 'location', 'class' => 'form-control', 'placeholder' => trans('forms.create_project_ph_location'))) !!} <i class="fa fa-fw {{ trans('forms.create_project_icon_location') }}" aria-hidden="true"></i>
                  </div>
                  @if ($errors->has('location'))
                    <span class="help-block">
                        <strong>{{ $errors->first('location') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="panel-footer">
                  <div class="form-actions">
                    {!! Form::button('<i class="fa fa-fw fa-save" aria-hidden="true"></i> Save Changes', array('class' => 'btn btn-success btn-block btn-save','type' => 'button', 'data-toggle' => 'modal', 'data-target' => '#confirmSave', 'data-title' => trans('modals.edit_project__modal_text_confirm_title'), 'data-message' => trans('modals.edit_project__modal_text_confirm_message'))) !!}
                  </div>
              </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

  @include('modals.modal-save')
  @include('modals.modal-delete')

@endsection

@section('footer_scripts')

  @include('scripts.delete-modal-script')
  @include('scripts.save-modal-script')
  @include('scripts.check-changed')

@endsection