
@section('template_title')
  Showing Projects
@endsection

@section('template_linked_css')
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
    <style type="text/css" media="screen">
        .projects-table {
            border: 0;
        }
        .projects-table tr td:first-child {
            padding-left: 15px;
        }
        .projects-table tr td:last-child {
            padding-right: 15px;
        }
        .projects-table.table-responsive,
        .projects-table.table-responsive table {
            margin-bottom: 0;
        }

    </style>
@endsection

@section('content')
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"><a href="{{ url('/home') }}" title="Go to Home" class="tip-bottom"><i class="fa fa-home"></i> Home</a> > <a href="#" class="current"><i class="fa fa-database"></i> My Project</a></div>
        <h1> @lang('projectsmanagement.showing-all-projects')</h1>
    </div>
    <div class="container-fluid">
    <hr>
        @include('partials.form-status')
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"><i class="fa fa-database"></i></span>
                        <h5> My Project </h5>
                        <div class="pull-right">
                            <a href="{{ url('projects/create') }}">
                                <span class="label label-info"><i class="fa fa-plus"></i> Create a New Project</span>
                            </a>
                        </div>
                    </div>
                    
                    <div class="widget-content nopadding">
                        @include('partials.search-projects-form')
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Projectname</th>
                                    <th>Type</th>
                                    <th>Location</th>
                                    <th>Created</th>
                                    <th>Update</th>
                                    <th colspan="3">Actions</th>
                                </tr>
                            </thead>
                            <tbody id="projects_table">
                                @foreach($projects as $project)
                                    <tr>
                                        <td>{{$project->id}}</td>
                                        <td>{{$project->projectname}}</td>
                                        <td>{{$project->type}}</td>
                                        <td>{{$project->location}}</td>
                                        <td class="hidden-sm hidden-xs hidden-md">{{$project->created_at}}</td>
                                        <td class="hidden-sm hidden-xs hidden-md">{{$project->updated_at}}</td>
                                        <td>
                                            {!! Form::open(array('url' => 'projects/' . $project->id, 'class' => '', 'data-toggle' => 'tooltip', 'title' => 'Delete')) !!}
                                                {!! Form::hidden('_method', 'DELETE') !!}
                                                {!! Form::button('<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Delete</span><span class="hidden-xs hidden-sm hidden-md"> project</span>', array('class' => 'btn btn-danger btn-sm','type' => 'button', 'style' =>'width: 100%;' ,'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete project', 'data-message' => 'Are you sure you want to delete this project ?')) !!}
                                            {!! Form::close() !!}
                                        </td>
                                        <td>
                                            <a class="btn btn-sm btn-success btn-block" href="{{ URL::to('projects/' . $project->id) }}" data-toggle="tooltip" title="Show">
                                                <i class="fa fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Show</span><span class="hidden-xs hidden-sm hidden-md"> Project</span>
                                            </a>
                                        </td>
                                        <td>
                                            <a class="btn btn-sm btn-info btn-block" href="{{ URL::to('projects/' . $project->id . '/edit') }}" data-toggle="tooltip" title="Edit">
                                                <i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Edit</span><span class="hidden-xs hidden-sm hidden-md"> Project</span>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tbody id="search_results"></tbody>
                        </table>
                    </div>
                    <div id="project_count project_pagination" class="form-actions">
                        {{ $projects->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    @include('modals.modal-delete')

@endsection

@section('footer_scripts')

    @include('scripts.delete-modal-script')
    @include('scripts.save-modal-script')
    {{--
        @include('scripts.tooltips')
    --}}

    {{-- @if(config('laravelprojects.enableSearchProjects')) --}}
            @include('scripts.search-projects')
    {{-- @endif --}}

@endsection
