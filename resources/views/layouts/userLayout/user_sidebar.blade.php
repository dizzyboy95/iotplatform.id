<!--sidebar-menu-->
<div id="sidebar">
  <ul>
    <li class="active"><a href="{{ url('/projects') }}"><i class="fa fa-database"></i> <span>Project</span></a> </li>
    <li><a href="{{ url('/account/'.Auth::user()->name.'/edit') }}"><i class="fa fa-user"></i> <span>Account</span></a> 
    <li><a href="{{ url('/documentation') }}" target="_blank"><i class="fa fa-book"></i> <span>Documentation</span></a> </li>
  </ul>
</div>
<!--sidebar-menu-->
