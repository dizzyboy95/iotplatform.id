<div class="form-horizontal">
    <div id="DataTables_Table_0_filter" class="pull-right">
        {!! Form::open(['route' => 'search-devices', 'method' => 'POST', 'role' => 'form', 'class' => 'needs-validation', 'id' => 'search_devices']) !!}
            {!! csrf_field() !!}
            <div class="input-group">
                {!! Form::text('device_search_box', NULL, ['id' => 'device_search_box', 'class' => 'form-control', 'placeholder' => trans('devicesmanagement.search.search-devices-ph'), 'aria-label' => trans('devicesmanagement.search.search-devices-ph'), 'required' => false]) !!}
                <a href="#" class="input-group-addon btn btn-warning clear-search" data-toggle="tooltip" title="@lang('ldevicesmanagement.tooltips.clear-search')" style="display:none;">
                    <i class="fa fa-times" aria-hidden="true"></i>
                    <span class="sr-only">
                        @lang('devicesmanagement.tooltips.clear-search')
                    </span>
                </a>
                <a href="#" class="input-group-addon btn btn-info" id="search_trigger">
                    <i class="fa fa-search" aria-hidden="true"></i>
                    <span class="sr-only">
                        {{  trans('devicesmanagement.tooltips.submit-search') }}
                    </span>
                </a>
            </div>
        {!! Form::close() !!}
    </div>
</div>