<div class="form-horizontal">
    <div id="DataTables_Table_0_filter" class="pull-right">
        {!! Form::open(['route' => 'search-projects', 'method' => 'POST', 'role' => 'form', 'class' => 'needs-validation', 'id' => 'search_projects']) !!}
            {!! csrf_field() !!}
            <div class="input-group">
                {!! Form::text('project_search_box', NULL, ['id' => 'project_search_box', 'class' => 'form-control', 'placeholder' => trans('projectsmanagement.search.search-projects-ph'), 'aria-label' => trans('projectsmanagement.search.search-projects-ph'), 'required' => false]) !!}
                <a href="#" class="input-group-addon btn btn-warning clear-search" data-toggle="tooltip" title="@lang('lprojectsmanagement.tooltips.clear-search')" style="display:none;">
                    <i class="fa fa-times" aria-hidden="true"></i>
                    <span class="sr-only">
                        @lang('projectsmanagement.tooltips.clear-search')
                    </span>
                </a>
                <a href="#" class="input-group-addon btn btn-info" id="search_trigger">
                    <i class="fa fa-search" aria-hidden="true"></i>
                    <span class="sr-only">
                        {{  trans('projectsmanagement.tooltips.submit-search') }}
                    </span>
                </a>
            </div>
        {!! Form::close() !!}
    </div>
</div>