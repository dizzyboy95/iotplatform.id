@extends('layouts.app3')

@section('template_title')
  Showing Status Sensor
@endsection

@section('template_fastload_css')
@endsection

@section('content')
<div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a href="{{ url('/home') }}" title="Go to Home" class="tip-bottom"><i class="fa fa-home"></i> Home</a> > <a href="{{ url('/projects') }}" title="Go to Create New Projects" class="tip-bottom"><i class="fa fa-database"></i> Projects</a> > <a href="{{url('/projects/'.$device->project_id)}}" title="Go to My Devices" class="tip-bottom"><i class="fa fa-laptop"></i> My Sensor</a> > <a href="#" class="current"><i class="fa fa-info-circle"></i> Status</a></div>
            <h1> @lang('statusesmanagement.showing-all-status')</h1>
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="span6">
                        <div class="widget-box">
                            <div class="widget-title"> <span class="icon"><i class="fa fa-info"></i></span>
                                <h5>{{ trans('devicesmanagement.devicesPanelTitle') }}</h5>
                            </div>
                            <div class="widget-content nopadding">
                                <ul class="recent-posts">
                                    <li>
                                        <div class="user-thumb"> <img width="150" height="150" alt="User" src="{{ asset('images/backend_images/Cube.jpeg') }}"> </div>
                                        <div class="article-post"> <span class="user-info"><h3> Sensor Name : {{$device->devicename}} </h3></span> </div>
                                        <p>Manage your sensor here. You can update value, show your value status detail, etc.</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="widget-box">
                            <div class="widget-title"> <span class="icon"><i class="fa fa-info"></i></span>
                                <h5>{{ trans('devicesmanagement.devicesPanelTitle') }}</h5>
                            </div>
                            <div class="widget-content nopadding">
                                <ul class="recent-posts">
                                    <li>
                                        <div class="article-post"> <span class="user-info"><h4> URL for connect your sensor to platform </h4></span> </div>
                                        <div class="article-post"> <span class="user-info"><h5> http://iotplatform.id/api/v1/status </h5></span> </div>
                                        <div class="article-post"> <span class="user-info"><h5> Device ID : {{$device->id}} </h5></span> </div>
                                        <div class="article-post"> <span class="user-info"><h5> Created : {{$device->created_at}} </h5></span> </div>
                                        <div class="article-post"> <span class="user-info"><h5> Last Update : {{$device->updated_at}} </h5></span> </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
        <hr>
            <div class="row">
                <div class="col-sm-6 col-md-12 text-center">
                    <div class="row justify-content-md-center">
                        <div class="col-8 col-md-auto">
                            <h3 class="text-muted">INFORMATION</h3>
                            <h4 class="text-muted">After this to check how to send your data, </h4>
                            <h4 class="text-muted">see step by step for sending data in <a href="{{ url('/documentation') }}" style="color: #0facf3;" target="_blank"><b>documentation</b></a></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

@endsection
