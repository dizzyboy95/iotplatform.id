@extends('layouts.app3')

@if (count($statuses) === 0)
    @section('content')

        @include('statusesmanagement.home-status')

    @endsection
    
@else
    @section('template_title')
        Showing Status Sensor
    @endsection

    @section('template_linked_css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
    <style type="text/css" media="screen">

        .statuses-table {
            border: 0;
        }
        .statuses-table tr td:first-child {
            padding-left: 15px;
        }
        .statuses-table tr td:last-child {
            padding-right: 15px;
        }
        .statuses-table.table-responsive,
        .statuses-table.table-responsive table {
            margin-bottom: 0;
        }

    </style>                
    @endsection

    @section('content')
    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a href="{{ url('/home') }}" title="Go to Home" class="tip-bottom"><i class="fa fa-home"></i> Home</a> > <a href="{{ url('/projects') }}" title="Go to Create New Projects" class="tip-bottom"><i class="fa fa-database"></i> Projects</a> > <a href="{{url('/projects/'.$device->project_id)}}" title="Go to My Devices" class="tip-bottom"><i class="fa fa-laptop"></i> My Sensor</a> > <a href="#" class="current"><i class="fa fa-info-circle"></i> Status</a></div>
            <h1> @lang('statusesmanagement.showing-all-status')</h1>
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="span6">
                        <div class="widget-box">
                            <div class="widget-title"> <span class="icon"><i class="fa fa-info"></i></span>
                                <h5>{{ trans('devicesmanagement.devicesPanelTitle') }}</h5>
                            </div>
                            <div class="widget-content nopadding">
                                <ul class="recent-posts">
                                    <li>
                                        <div class="user-thumb"> <img width="150" height="150" alt="User" src="{{ asset('images/backend_images/Cube.jpeg') }}"> </div>
                                        <div class="article-post"> <span class="user-info"><h3> Sensor Name : {{$device->devicename}} </h3></span> </div>
                                        <p>Manage your sensor here. You can update value, show your value status detail, etc.</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="widget-box">
                            <div class="widget-title"> <span class="icon"><i class="fa fa-info"></i></span>
                                <h5>{{ trans('devicesmanagement.devicesPanelTitle') }}</h5>
                            </div>
                            <div class="widget-content nopadding">
                                <ul class="recent-posts">
                                    <li>
                                        <div class="article-post"> <span class="user-info"><h4> URL for connect your sensor to platform </h4></span> </div>
                                        <div class="article-post"> <span class="user-info"><h5> http://iotplatform.id/api/v1/status </h5></span> </div>
                                        <div class="article-post"> <span class="user-info"><h5> Device ID : {{$device->id}} </h5></span> </div>
                                        <div class="article-post"> <span class="user-info"><h5> Created : {{$device->created_at}} </h5></span> </div>
                                        <div class="article-post"> <span class="user-info"><h5> Last Update : {{$device->updated_at}} </h5></span> </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
        <hr>
            
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"> <i class="fa fa-line-chart"></i> </span>
                            <h5>Graph</h5>
                        </div>
                        <div class="widget-content">
                            <div id="chart" style="width: 100%; height: 400px"></div>
                        </div>
                        <div class="form-actions">
                        {!! Form::model($device, array('action' => array('DevicesManagementController@update', $device->id), 'method' => 'PUT', 'class' => 'form-horizontal')) !!}

                            {!! csrf_field() !!}

                            @if($device->onoff === 1)
                            <input type="hidden" name="onoff" value=0>
                                {!! Form::button(
                                        '<i class="fa fa-fw fa-toggle-off" aria-hidden="true"></i> ' . trans('profile.submitOFFButton'),
                                            array(
                                                'class'        => 'btn btn-danger',
                                                'disabled'     => false,
                                                'type'         => 'submit',
                                                'data-toggle'  => 'modal'
                                )) !!}
                                <div class="pull-right">
                                    {!! Form::button(
                                            '<i class="fa fa-fw fa-toggle-on" aria-hidden="true"></i> ' . trans('profile.submitONButton'),
                                                array(
                                                    'class'         => 'btn btn-success',
                                                    'disabled'     => true,
                                                    'type'          => 'submit',
                                                    'data-toggle'   => 'modal'
                                    )) !!}
                                </div>
                            @else
                            <input type="hidden" name="onoff" value=1>
                                {!! Form::button(
                                        '<i class="fa fa-fw fa-toggle-off" aria-hidden="true"></i> ' . trans('profile.submitOFFButton'),
                                                array(
                                                    'class'        => 'btn btn-danger',
                                                    'disabled'     => true,
                                                    'type'         => 'submit',
                                                    'data-toggle'  => 'modal'
                                )) !!}
                                <div class="pull-right">
                                    {!! Form::button(
                                            '<i class="fa fa-fw fa-toggle-on" aria-hidden="true"></i> ' . trans('profile.submitONButton'),
                                                array(
                                                    'class'         => 'btn btn-success',
                                                    'disabled'      => false,
                                                    'type'          => 'submit',
                                                    'data-toggle'   => 'modal'
                                    )) !!}
                                </div>
                            @endif
                        {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"><i class="fa fa-info-circle"></i></span>
                            <h5> Value Status </h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered data-table">
                                <thead>
                                    <tr>
                                        <th>Value</th>
                                        <th>satuan</th>                                     
                                    </tr>
                                </thead>
                                <tbody id="Statuses_table">
                                    @foreach($statuses as $status)
                                        <tr>
                                            <td>{{$status->nilai}}</td>
                                            <td>{{$status->satuan}}</td>   
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tbody id="search_results"></tbody>
                            </table>
                        </div>
                        <div id="status_count status_pagination" class="form-actions">
                            {{ $statuses->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        @include('modals.modal-form')

    
    @endsection

@section('footer_scripts')

    @include('scripts.form-modal-script')

@endsection

        <script src="{{ asset('amcharts/amcharts.js') }}" type="text/javascript"></script>
        <script src="{{ asset('amcharts/serial.js') }}" type="text/javascript"></script>
        <script src="{{ asset('amcharts/themes/dark.js') }}" type="text/javascript"></script>
        <script src="{{ asset('amcharts/amcharts.js') }}" type="text/javascript"></script>
        <script src="{{ asset('amcharts/xy.js') }}" type="text/javascript"></script> 

    <script>
    var data = {!! $coba !!}
    var chart = AmCharts.makeChart("chart", {
        "type": "serial",
       
        "theme": "light",
        "dataDateFormat": "H-i-s",
        "dataProvider": data,

        "valueAxes": [{
            "maximum": 100,
            "minimum": 0,
            "axisAlpha": 0,
            "title": "",
            "guides": [{
                "fillAlpha": 0.1,
                "fillColor": "#CC0000",
                "lineAlpha": 0,
                "toValue": 120,
                "value": 0
            }, {
                "fillAlpha": 0.1,
                "fillColor": "#0000cc",
                "lineAlpha": 0,
                "toValue": 200,
                "value": 120
            }]
        }],
        "graphs": [{
            "bullet": "round",
            "valueField": "nilai",
            "balloonText": "<p style='text-align: center;'><b>[[update_at]]</b><br>Value : <b>[[nilai]]</b> [[satuan]]", 
            
        }],
        "chartCursor": {
            "cursorAlpha": 0,
            "zoomable":false,
            "valueZoomable":true
        },
        "categoryField": "created_at",
        "valueScrollbar":{

        }
    });
    </script>
@endif
