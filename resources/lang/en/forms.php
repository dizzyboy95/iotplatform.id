<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match forms.
    |
    */

    // CREATE NEW USER FORM
    'create_user_label_email' => 'User Email',
    'create_user_ph_email'    => 'User Email',
    'create_user_icon_email'  => 'fa-envelope',

    'create_user_label_username' => 'Username',
    'create_user_ph_username'    => 'Username',
    'create_user_icon_username'  => 'fa-user',

    'create_user_label_firstname' => 'First Name',
    'create_user_ph_firstname'    => 'First Name',
    'create_user_icon_firstname'  => 'fa-user',

    'create_user_label_lastname' => 'Last Name',
    'create_user_ph_lastname'    => 'Last Name',
    'create_user_icon_lastname'  => 'fa-user',

    'create_user_label_password' => 'Password',
    'create_user_ph_password'    => 'New Password',
    'create_user_icon_password'  => 'fa-lock',

    'create_user_label_pw_confirmation' => 'Confirm Password',
    'create_user_ph_pw_confirmation'    => 'Confirm New Password',
    'create_user_icon_pw_confirmation'  => 'fa-lock',

    'create_user_label_location' => 'User Location',
    'create_user_ph_location'    => 'User Location',
    'create_user_icon_location'  => 'fa-map-marker',

    'create_user_label_bio' => 'User Bio',
    'create_user_ph_bio'    => 'User Bio',
    'create_user_icon_bio'  => 'fa-pencil',

    'create_user_label_twitter_username' => 'User Twitter Username',
    'create_user_ph_twitter_username'    => 'User Twitter Username',
    'create_user_icon_twitter_username'  => 'fa-twitter',

    'create_user_label_github_username' => 'User GitHub Username',
    'create_user_ph_github_username'    => 'User GitHub Username',
    'create_user_icon_github_username'  => 'fa-github',

    'create_user_label_career_title' => 'User Occupation',
    'create_user_ph_career_title'    => 'User Occupation',
    'create_user_icon_career_title'  => 'fa-briefcase',

    'create_user_label_education' => 'User Education',
    'create_user_ph_education'    => 'User Education',
    'create_user_icon_education'  => 'fa-graduation-cap',

    'create_user_label_role' => 'User Role',
    'create_user_ph_role'    => 'Select User Role',
    'create_user_icon_role'  => 'fa-shield',

    'create_user_button_text' => 'Create New User',

    // EDIT USER AS ADMINISTRATOR FORM
    'edit-user-admin-title' => 'Edit User Information',

    'label-username' => 'Username',
    'ph-username'    => 'Username',

    'label-useremail' => 'User Email',
    'ph-useremail'    => 'User Email',

    'label-api_token' => 'Key Access',
    'ph-api_token'    => 'Key Access',

    'label-userrole_id' => 'User Access Level',
    'option-label'      => 'Select a Level',
    'option-user'       => 'User',
    'option-editor'     => 'Editor',
    'option-admin'      => 'Administrator',
    'submit-btn-text'   => 'Edit the User!',

    'submit-btn-icon' => 'fa-save',
    'username-icon'   => 'fa-user',
    'useremail-icon'  => 'fa-envelope-o',

    // CREATE NEW PROJECT FORM
    'create_project_label_projectname' => 'Project',
    'create_project_ph_projectname'    => 'Ex: SmartLamp, SmartGarage',
    'create_project_icon_projectname'  => 'fa-database',

    'create_project_label_type' => 'Type',
    'create_project_ph_type'    => 'Ex: Sensor, Actuator',
    'create_project_icon_type'  => 'fa-tags',

    'create_project_label_location' => 'Location',
    'create_project_ph_location'    => 'Ex: Home, Office',
    'create_project_icon_location'  => 'fa-map-marker',

    'create_project_button_text' => 'Create New Project',


    // EDIT PROJECT AS EVERYTHING FORM
    'edit-project-admin-title' => 'Edit Project Information',

    'label-projectname' => 'Projectname',
    'ph-projectname'    => 'Projectname',

    'label-projecttype' => 'Type',
    'ph-projecttype'    => 'Ex: Sensor, Actuator',

    'label-projectrole_id' => 'Project Access Level',
    'option-label'      => 'Select a Level',
    'option-project'       => 'Project',
    'option-editor'     => 'Editor',
    'option-admin'      => 'Administrator',
    'submit-btn-text'   => 'Edit the Project!',

    'submit-btn-icon' => 'fa-save',
    'projectname-icon'   => 'fa-Project',
    'projecttype-icon'  => 'fa-tags',

    // ADD NEW DEVICE FORM
    'create_device_label_devicename' => 'Sensor Name',
    'create_device_ph_devicename'    => 'Ex : Temperature Sensor',
    'create_device_icon_devicename'  => 'fa-tablet',
    'create_device_button_text' => 'Create New Sensor',


    // EDIT DEVICE AS EVERYTHING FORM
    'edit-device-admin-title' => 'Edit Device Information',

    'label-devicename' => 'Devicename',
    'ph-devicename'    => 'Devicename',

    'label-devicerole_id' => 'Device Access Level',
    'option-label'      => 'Select a Level',
    'option-device'       => 'Device',
    'option-editor'     => 'Editor',
    'option-admin'      => 'Administrator',
    'submit-btn-text'   => 'Edit the Device!',

    'submit-btn-icon' => 'fa-save',
    'devicename-icon'   => 'fa-laptop',
    'devicetype-icon'  => 'fa-tags',

    // ADD VALUE STATUS FORM
    'create_status_label_nilai'  => 'Value',
    'create_status_ph_nilai'     => 'Ex : 10, 20, 30',
    'create_status_label_satuan' => 'Unit',
    'create_status_ph_satuan'    => 'Ex : Celcius, Meter, Percent',
    'create_status_icon_nilai'   => 'fa-plus-circle',
    'create_status_icon_satuan'  => 'fa-percent',
    'create_status_button_text'  => 'Add New Value Status',

];