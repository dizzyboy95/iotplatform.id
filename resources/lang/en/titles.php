<?php

return [

    'app'               => 'Tugas Akhir',
    'app2'              => 'judul',
    'home'              => 'Home',
    'login'             => 'Login',
    'logout'            => 'Logout',
    'sensorON'          => 'Sensor ON',
    'sensorOFF'         => 'Sensor OFF',
    'register'          => 'Register',
    'resetPword'        => 'Reset Password',
    'toggleNav'         => 'Toggle Navigation',
    'profile'           => 'Profile',
    'editProfile'       => 'Edit Profile',
    'createProfile'     => 'Create Profile',

    'activation'        => 'Registration Started  | Activation Required',
    'exceeded'          => 'Activation Error',

    'editProfile'       => 'Edit Profile',
    'createProfile'     => 'Create Profile',
    'adminUserList'     => 'Users Administration',
    'adminEditUsers'    => 'Edit Users',
    'adminNewUser'      => 'Create New User',

    'adminThemesList'   => 'Themes',
    'adminThemesAdd'    => 'Add New Theme',

    'adminLogs'         => 'Log Files',
    'adminActivity'     => 'Activity Log',
    'adminPHP'          => 'PHP Information',
    'adminRoutes'       => 'Routing Details',

    'activeUsers'       => 'Active Users',
];
