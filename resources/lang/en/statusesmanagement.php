<?php

return [

    // Titles
    'showing-all-status'     => 'Showing All Value Status',

    // Flash Messages
    'createSuccess'   => 'Successfully created status! ',
    'updateSuccess'   => 'Successfully updated status! ',
    'deleteSuccess'   => 'Successfully deleted status! ',
    'deleteSelfError' => 'You cannot delete yourself! ',

    // Show device Tab
    'statusPanelTitle'     => 'Value Status Information',
    'labelDeviceName'       => 'Devicename:',
    'labelStatus'            => 'Status:',
    'labelAccessLevel'       => 'Access',
    'labelPermissions'       => 'Permissions:',
    'labelIpEmail'           => 'Email Signup IP:',
    'labelIpEmail'           => 'Email Signup IP:',
    'labelIpConfirm'         => 'Confirmation IP:',
    'labelIpSocial'          => 'Socialite Signup IP:',
    'labelIpAdmin'           => 'Admin Signup IP:',
    'labelIpUpdate'          => 'Last Update IP:',
    'labelDeletedAt'         => 'Deleted on',
    'labelIpDeleted'         => 'Deleted IP:',
    'devicesDeletedPanelTitle' => 'Deleted Device Information',
    'devicesBackDelBtn'        => 'Back to Deleted Devices',

    'successRestore'    => 'Value Status successfully restored.',
    'successDestroy'    => 'Value Status record successfully destroyed.',
    'errorValueNotFound' => 'Value Status not found.',

    'labelDeviceLevel'  => 'Level',
    'labelDeviceLevels' => 'Levels',

    'statuses-table' => [
        'caption'   => '{1} :statusescount device total|[2,*] :statusescount total value status',
        'id'        => 'ID',
        'name'      => 'Name',
        'actions'   => 'Actions',
        'updated'   => 'Updated',
    ],

    'buttons' => [
        'add-new'    => '<span class="hidden-xs hidden-sm">New Value Status</span>',
        'delete'        => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Delete</span><span class="hidden-xs hidden-sm hidden-md"> Value Status</span>',
        'show'          => '<i class="fa fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Show</span><span class="hidden-xs hidden-sm hidden-md"> Value Status</span>',
        'delete-status'   => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs">Delete</span><span class="hidden-xs"> Value Status</span>',
    ],

    'tooltips' => [
        'delete'        => 'Delete',
        'show'          => 'Show',
        'add-new'       => 'Add New Value Status',
    ],

    'messages' => [
        'statusNameTaken'          => 'Devicename is taken',
        'statusNameRequired'       => 'Devicename is required',
        'status-creation-success'  => 'Successfully created device!',
        'update-status-success'    => 'Successfully updated device!',
        'delete-success'           => 'Successfully deleted the device!',
        'cannot-delete-yourself'   => 'You cannot delete yourself!',
    ],

    'modals' => [
        'delete_device_message' => 'Are you sure you want to delete :device?',
        
    ],
];
